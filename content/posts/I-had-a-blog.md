+++
title = "[EN] Once upon I Had a blog…"
date = "2024-11-25T00:39:13+01:00"
author = "Hussein Kasem"
showFullContent = false
readingTime = false
+++
Once upon a time I had my own little corner on the vast Internet. I started it, and then buried it somewhere deep inside my mind… 

You may wonder (probably not, but my inner self does):
* What made you remember it now?
* Will this time be any different?

Let me answer myself (hopefully my future self will not regret this ;P)
#### What made you remember it now:
This domain was expiring on August while I was on holiday and I got the renewal notification, and what happened is:
  * I tried to write something and failed: 
    * Project was not building, after renaming `master` to `main`
        * Hugo: was multiple versions ahead
        * Terminal theme: was outdated and unmaintained
  * I updated everything and got it up and running

That was in August and nothing changed, no updates at all. However I had a background thread busy with this topic, wondering how to restart and how to keep my motivation to keep a pace.

Now we're in November, I have some ideas, one of them this blog post, and what happened? A new versions of Hugo were released, and they weren't compatible with the theme I was using which in August was not being actively maintained, although that I was **very** lucky, as maintainers got back on August 21st 

I've updated the theme, but it was still not working, the Docker image I was using wasn't up to date either, Thankfully I found an image, got it  up and running again and now it is time to focus on **write**, and **share** 🎉 

#### Will this time be any different: 
Yes! But does it matter? At the end is my own place, and I can find it whenever I feel like I want to write, at some point I was also wondering, am I adding something? Or just emitting CO<sub>2</sub> for nothing?

And I concluded that it doesn't matter, it is adding something, for me, it is helping me with writing and getting some fun.

### What happened during all that time?
* COVID-19
* I've moved to a new job after 6.5 years to a completely new space, from privacy to travel, from lead to plain engineer
  * And finally I am not the more experienced **Android** developer in the room (it was happening even during my internship)
  * With this I've been on: e-learning, e-health, consultancy, privacy and travel domains
* I've battled with [Rust multiplatform for Android (and iOS)][sequoia-android-readme] and moved away from it
* I've been teaching again
* Gave my first (and second) talk in an internal company conference
* I've got back to facilitating some meetups
* I crossed the 30-year threshold
* I've learned tons of things to share here, but not yet :D
  *  And not only tech related
* Last but not least: A cat got me and now she is my pairing buddy

### What's next?
My plan is to get comfortable writing, try to move out of the shadows. I've never felt comfortable openly sharing this kind of stuff but it is time to shed some light, crosspost, maybe start to build a personal brand lets see where we do arrive. 

Who knows, maybe soon I will even start to ask to follow and subscribe 🙈

[sequoia-android-readme]: https://gitlab.com/sequoia-pgp/sequoia/-/blob/main/README.android
