---
title: "❄️ Advent of Code 2024 🚀"
date: 2024-12-01T00:55:40+01:00
---

December is here 🎉, and that means winter⛄, holidays, and all the good stuff. It also means [**Advent of code**][1] is back! If you wonder what it is, it’s a daily coding challenge running from December 1st to 25th (at 6 AM CET), where you solve puzzles programmatically 👨‍💻. There aren’t any rules, just write your code, run it with your input, and paste the output. Simple, fun, and oddly satisfying.

This year, I’m jumping in again. My plan is to start with today’s puzzle and keep going until I hit a wall or get overwhelmed. Let’s see how far I can make it! (Wish me luck 🍀)

I’m kicking things off with a Kotlin [repository][2] to share my solutions, but who knows? I might try out some other languages along the way. It’s all part of the fun.

Here’s to another round of Advent of Code. Let’s do this!

[1]: https://adventofcode.com/2024
[2]: https://github.com/helbaroudy/aoc-2024-amper
