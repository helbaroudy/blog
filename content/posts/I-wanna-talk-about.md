---
title: "[EN] I Wanna Talk About..."
date: 2019-02-28T01:16:31+01:00
author: Hussein Kasem
---
Since my last post I am wondering about how to organize this blog, and for now those are the main "Topics":

 *  **TIL**: Today I leanerned
 *  **Thoughts**: Just general thoughts
 *  **Unnamed**: Posts about programing, tutorials, tests I played, etc
 *  **My path**: Things related to books I'm reading related to my career
 *  **Unnamed 2**: A section with Spanish content.


Looking at the list we can se two things:

 1. **It is still not clear the list of Topic**: Yes, I'm still wondering and thinking about it and I want to write it down as a reminder or guide, to look at it later and start writing instead of just wondering.
 2. **I'm changing one of my decisions**: On [my first post] (../hello-world/) I said I will write in English and I'm commited with doing that except sometime I feel the need to write in Spanish for things I'm not finding in English, or just I do not see myself writing that post in English and instead of not writting it I will write it in Spanish.   


Note: Although I did not want to publish more until I clarify my mind and come with a structure while I was talking about this we a good friend he told me "Just write and organize later" and here I am one week later writing again.
