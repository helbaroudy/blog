---
title: "[EN] Hello World"
date: 2019-02-14T23:37:28+01:00
author: Hussein Kasem
---
"Today" (today was 13/feb) while I was [building](https://xkcd.com/303/) and checking twitter I was thinking why I do not build my own blog (again)...

And then the answer was clear *because you do not have anything to offer, everything is already there*

Then while I was commuting back the idea was still there and I decided to build a blog, but this time not with the idea of offering something, just as my **own corner** on the Internet (or "the cloud") where I write for myself just because I always have project/ideas on mind, but I never write them or find time to finish them.

And now I'm here and writing my "Hello World" before having the blog, and there is a reason for that, I want to prove to myself that I am able to write content, and not just "build" a blog.

## Let's start
### Language
Looks like finally I'm convinced to start, but this is just the beginning other topic I was wondering about was, in which language shall I write and why. At the end I can choose between two languages English or Spanish:
  * **English**: Is not my native language but I have tu use it almost daily because of my job, also I always feel stressed when I have to use it, as for me is not natural. And I decided to choose it as a way to force me to practice it, improve it and hopefully forget the shame I feel every-time I use it because my skill. 
  * **Spanish**: This would have been the easy choice as I am pretty comfortable writing it, but I had a little voice in my head teasing me saying: "is too easy".

  Then first decision done **I will write in English**

  Note: Probably I will end with posts on both languages or even others

### Software
Great! I started to write in English, but I want to publish this content somewhere using something, and then I started to think about *using something* from building my own blogging software using [Django Framework](https://www.djangoproject.com/), use some ready [CMS](https://en.wikipedia.org/wiki/Content_management_system), use some service like [Medium](https://medium.com) or even write plain HTML. 

And again a decision needs to be made, and my mind is just jumping from one topic to other I had to stop and think again, what I wanna do and what I need.

* What I wanna do: Write posts
* What I need: Some place to publish this thoughts 

This is for **me** I do not need a dynamic system with user management, backend and all of this fancy and modern things I just need something static. 

I want to write, I would have had a lot of fun building and designing my own blogging software even if is not a dynamic system and it is a Static Site Generator but then again I would get lost on trying to build the perfect thing and never considere it done .o0(Looks like I am starting to know myself...). 

Then almost against my will **I will use a Static Site generator**

And is the moment to choose one I heard about a couple of them but it's always good to google and see what is out there, what I found is a [list of static generators](https://www.staticgen.com/). 

After spending some time looking at them I narrow it down two three options, two I heard about before and one from the site

  * [yblog](https://blog.fdik.org/yblog2.tar.bz2) and [YML](https://fdik.org/yml/):  This does not publicize itself a static site generator but in fact it is, is a tool written using [YML](https://fdik.org/yml/) to generate HTML, it is lightweight (just a few lines of code) it can be extended and is quick to use, but is a tailor made solution and if I wanna personalise it I would have to rewrite part of it. 
  * [Jekyll](https://jekyllrb.com/): I've heard about this one before, because it is built in GitHub, is what Github pages use. But after looking at it I did not like it, don't know really why, but was not feeling confortable reading the samples.
  * [Hugo](https://gohugo.io/): I just discovered this one today, and reading the samples was feeling ok, and I choose this one.

Probably is not the best way to choose, but if I do not like it I can change it later.

I will use [Hugo](https://gohugo.io/) to generate my blog

### Naming and authorship
When I started blogs before I was always avoiding my real name to publish publicly to Internet it is always hard to use my real name and photo, it makes me feel like naked. But if I want to make this **my** corner this time I need to asume it is myself, and no body cares about my name is just a name and because of that for first time I will blog using my real name .o0(lets hope it does not bit me).

I can sound banal but just putting my name on it feels strange. 

This time the main idea is about having a corner and because of that I will name it Kasem Corner, I could name it Kasem blog, but while I am writing this post the idea of a corner is becoming lovely. 

**https://corner.kasem.tech**

### Contents 
And the hardest part (at least for me) when it becomes to blogging, write about what, as I said I have nothing to offer. 

And because is for me **I will write about Random**.

### Summary (decisions taken)
1. Build a blog
2. Write it in English
3. Use Static Site Generator 
4. Start with [Hugo](https://gohugo.io/)
5. Use real name: Hussein Kasem Corner
6. Content: Random
