---
title: "[EN] Hello Again"
date: 2019-02-15T01:29:59+01:00
author: Hussein Kasem 
---
On my [previous post](../hello-world) I decided to build a blog, but forget about a quite important part, the deployment (just little details) and which dev tools I will use

## Dev tools
Here mi mind instead of just picking the tools just started to wonder again about the tooling mainly about what to use to write, an IDE a plain editor, and which [SCM] (https://en.wikipedia.org/wiki/Software_configuration_management).

On the IDE front I love Intellij IDE's, but it sounds too much, then I will just use Visual Studio Code && Vim.

On the SCM front I'm taking git, while I'm very tempted to use Mercurial. I use both on my work and I managed to miss git when using hg, but also miss hg when using git.

Related with the hosting I was between setup a VPS which git to host the code but at the end I opted for https://gitlab.com + sign commits. This option was chosen just by convenience and to avoid doing devops for this project.

## Deployment
In my intend of avoiding devops on this project as much as posible I found that gitlab has gitlab pages, which can be used to serve static content which makes it an easy choice

## After deployment
And now what? the blog is online, from time to time I publish, but there is something missing?, there is a little voice saying yes!, the analytics you have two know which people you are reaching, if they spent time here or not, but NOT (at least for now).

I will not put analytics in this blog for two reason: 
  
  1. I do **not want any cookies warning** or adding a privacy policy 
  2. I want to enjoy writing without caring if someone reads or not, if someone does great, if not great too. In this case _Ignorance is bliss_


Hopefully next post will come after going public. 
