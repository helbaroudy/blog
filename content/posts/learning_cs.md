---
title: "[EN] Why does CS expect students to learn to write before they learn to read?"
date: 2019-02-28T02:02:24+01:00
author: Hussein Kasem
---
**Disclaimer: This post is probably very naive**

Today I was searching for an old email, and while doing that I found this quote hidden on a signature after the legal/privacy note (unfortunately I was not able to find the original author of this quote).

 > Why does CS expect students to learn to write before they learn to read?

This quote was in a email from 2010 and at that time I did not pay attention on it but today it was like blinking with neons.

Why? I assume that happened for two reasons: on 2010 I did not had started to code professionally yet, and I did not know that it is so true. This time I could not avoid think about it...

When we learn or teach to code, we start giving the basics like syntax, keywords, etc. And then expect to get **written** code by just reading little examples (or not even that) therefore we do not read much code when we start, we don't start by reading a "Hello World" we start writing it.

And now I am thinking if we read more code (in the language we are trying to learn) before writing it would...

* ... make us develop some sensitivity on the language conventions and styling?
* ... make us aware that we are authors writing code which will be read by other
* ... make us learn it faster?

And for doing that is enough just providing beautiful and well written pieces of code, to read and analyze before starting to code? Or what is needed?

For finishing I would like to add another quote:

> Indeed, the ratio of time spent reading versus writing is well over 10 to 1 -- <cite>[Robert C. Martin][1]</cite>


**Then we start learning code by writing it to end reading 10 times more code than the code we write.**

#### One last thing...

 When I say _to code_ I mean write code in concrete language, which is not the same as programming and one last quote:

> We did not taught you how to program C++ we taught you how to program!.

This was heard @[FIB](https://www.fib.upc.edu/) when suddenly we were in a new programming subject(or course) and been asked to do the assignments using Java without any introduction or class time assigned to learn/teach java a mate complained about not been taught Java, and this was answer.

And from here we can extract the difference between coding and programming (at least for me), programing concepts are absolutely transversal like control-flow structures, algorithms, etc. And coding is just use a a concrete language to apply programming knowledge.

[1]:https://www.goodreads.com/quotes/835238-indeed-the-ratio-of-time-spent-reading-versus-writing-is

---
This post was saves as a draft since 28/02/2019 to today (11/03/2019) and I still don't like it, I feel there is something wrong with it, said that I am publishing to keep going :D.
