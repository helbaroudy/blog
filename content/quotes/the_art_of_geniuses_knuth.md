---
title: "... the art of geniuses ..."
date: 2024-11-26T00:25:13+01:00
---

> People think that computer science is the art of geniuses but the actual reality is the opposite, just many people doing things that build on eachother, like a wall of mini stones. -- <cite>[Donald Knuth][1]</cite>

<br />

<!--more-->
This is one of my all-time favourite quotes, specially coming from a genius like [Knuth][2] which I can't do justice to with words. He is the inventor of TeX, author of `The art of computer programming`, and the guy who sends you a $2.56 reward [check][3] if you find typographical errors or mistakes on his books.

Besides who said it, I think that it captures perfectly the software engineering world. We are not titans performing heroic feats to move forward. We are ants, working collaboratively in teams, building walls stone by stone.

[1]: https://www.brainyquote.com/quotes/donald_knuth_202347
[2]: https://en.wikipedia.org/wiki/Donald_Knuth
[3]: https://en.wikipedia.org/wiki/Knuth_reward_check
